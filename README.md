# React Developer Test Project at MustFin

## Getting Started

```bash
# Clone the repository
git clone https://gitlab.com/bipro10/mustfin-react-test.git
# Install dependencies
$ npm install
# First, run the development server:
$ npm run dev
```
