const siteUrl = process.env.NEXT_PUBLIC_APP_URL || "http://localhost:3000";

export const siteConfig = {
  name: "Mustfin React",
  description: "React Developer Test Project at MustFin",
  url: siteUrl,
  ogImage: `${siteUrl}/og.png`,
  author: {
    name: "Biprodas Roy",
    email: "biprodas.cse@gmail.com",
    website: "",
  },
  links: {
    linkedIn: "https://www.linkedin.com/in/biprodas-roy",
    github: "https://github.com/biprodas/mustfin-react-test",
  },
};

export type SiteConfig = typeof siteConfig;
